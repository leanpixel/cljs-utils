(ns cljs-utils.xhr
  (:require [goog.events :as events]
            [cognitect.transit :as transit]
            [clojure.string :as string]
            [cljs.reader :as reader])
  (:import [goog.net XhrIo EventType]))

(def formats
  {"application/transit+json"
   {:read-fn (fn [x]
               (transit/read (transit/reader :json) x))
    :write-fn (fn [x]
                (transit/write (transit/writer :json) x))}
   "application/json"
   {:read-fn (fn [x]
               (transit/read (transit/reader :json) x))
    :write-fn (fn [x]
                (transit/write (transit/writer :json-verbose) x))}
   "application/edn"
   {:read-fn reader/read-string
    :write-fn pr-str}
   "text/plain"
   {:read-fn identity
    :write-fn identity}})

(defn request
  "Send an xhr request with the given data as EDN
  Implementation taken from om-sync."
  [{:keys [method url accept content-type data on-complete on-error on-progress auth]}]
  (let [xhr (XhrIo.)
        write-fn (or (get-in formats [content-type :write-fn]) identity)
        headers {"Content-Type" content-type
                 "Accept" accept}
        headers (clj->js
                  (if auth
                    (assoc headers "Authorization" (str "Basic " (js/btoa (string/join ":" auth))))
                    headers))]
    (when on-progress
      (events/listen xhr EventType.PROGRESS
        (fn [e] (on-progress e))))
    (when on-complete
      (events/listen xhr EventType.SUCCESS
        (fn [e]
          (let [read-fn (or (get-in formats [(first (string/split (.getResponseHeader xhr "Content-Type") ";")) :read-fn])
                            (get-in formats [accept :read-fn])
                            identity)]
            (on-complete (read-fn (.getResponseText xhr)))))))
    (when on-error
      (events/listen xhr EventType.ERROR
        (fn [e]
          (let [read-fn (or (get-in formats [(.getResponseHeader xhr "Content-Type") :read-fn]) identity)]
            (on-error {:error
                       (read-fn (.getResponseText xhr))
                       :status (.getStatus xhr)})))))
    (.send xhr url (.toUpperCase (name method)) (when data (write-fn data)) headers 5000)))
