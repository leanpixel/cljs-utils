(ns cljs-utils.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [goog.events :as events]
            [cljs.core.async :refer [chan timeout alts! put!]]
            [clojure.string :as string]
            [cljs.reader :as reader])
  (:import [goog.net XhrIo EventType]))

(defn string-to-date
  "attempts to cast a string as a date, or returns nil"
  [string]
  (let [epochtime (js/Date.parse string)]
    (when (and
            (< 8 (count string)) ; require a min length so that small numbers aren't classified as dates
            (not (js/isNaN epochtime)) )
      (js/Date. epochtime))))

(defn flip
  "Partially apply the function f to the given args, which will come after the
  next args.  i.e. ((flip vector 3 4) 1 2) => [1 2 3 4]"
  [f & a]
  (fn [& b]
    (apply f (concat b a))))

(defn dissocv
  "Remove an element from a vector at the given index, returning the new vector"
  [v i]
  (vec (concat (subvec v 0 i) (subvec v (inc i) (count v)))))

(defn debounce
  "Given the input channel source and a debouncing time of msecs, return a new
  channel that will forward the latest event from source at most every msecs
  milliseconds"
  [source msecs]
  (let [out (chan)]
    (go
      (loop [state ::init
             lastv nil
             chans [source]]
        (let [[_ threshold] chans]
          (let [[v sc] (alts! chans)]
            (condp = sc
              source (recur ::debouncing v
                            (case state
                              ::init (conj chans (timeout msecs))
                              ::debouncing (conj (pop chans) (timeout msecs))))
              threshold (do (when lastv
                              (put! out lastv))
                            (recur ::init nil (pop chans))))))))
    out))

(defn link-attr?
  "Is the given attribute a reference?"
  [attr]
  (= "entity.ref" (namespace attr)))
